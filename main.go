package main

import "fmt"

/**
The task is to implement the Library interface (you can do that in this file directly).
- No database or any other storage is required, just store data in memory
- No any smart search, use string function contains (case sensitive/insensitive - does not matter)
– Performance optimizations are optional
*/

// Book ...
type Book struct {
	id, name, author string
}

// Library ...
type Library interface {
	/**
	  Adds a new book object to the Library.
	  - Parameter book: book to add to the Library
	  - Returns: false if the book with same id already exists in the Library, true – otherwise.
	*/
	addNewBook(book Book) bool

	/**
	  Deletes the book with the specified id from the Library.
	  - Returns: true if the book with same id existed in the Library, false – otherwise.
	*/
	deleteBook(id string) bool

	/**
	  - Returns: 10 book names containing the specified string. If there are several books with the same name, author's name is appended to book's name.
	*/
	listBooksByName(searchString string) []string

	/**
	  - Returns: 10 book names whose author contains the specified string, ordered by authors.
	*/
	listBooksByAuthor(searchString string) []string
}

func test(lib Library) {
	assert(!lib.deleteBook("1"))
	assert(lib.addNewBook(Book{id: "1", name: "1", author: "Lex"}))
	assert(!lib.addNewBook(Book{id: "1", name: "1", author: "Lex"}))
	assert(lib.deleteBook("1"))
	assert(lib.addNewBook(Book{id: "4", name: "Name1", author: "Lex3"}))
	assert(lib.addNewBook(Book{id: "3", name: "Name3", author: "Lex2"}))
	assert(lib.addNewBook(Book{id: "2", name: "Name2", author: "Lex2"}))
	assert(lib.addNewBook(Book{id: "1", name: "Name1", author: "Lex1"}))
	byNames := lib.listBooksByName("Name")
	assert(contains(byNames, "Lex3 - Name1"))
	assert(contains(byNames, "Name2"))
	assert(contains(byNames, "Name3"))
	assert(contains(byNames, "Lex1 - Name1"))
	byAuthor := lib.listBooksByAuthor("Lex")
	assert(byAuthor[0] == "Name1")
	assert(byAuthor[1] == "Name2" || byAuthor[1] == "Name3")
	assert(byAuthor[2] == "Name2" || byAuthor[2] == "Name3")
	assert(byAuthor[3] == "Name1")
}

func contains(arr []string, str string) bool {
	for _, a := range arr {
		if a == str {
			return true
		}
	}
	return false
}

func assert(condition bool) {
	if !condition {
		panic("Assertion failed")
	}
}

func main() {
	libra := NewLibrary()

	test(libra)

	fmt.Print("tests passed")
}
