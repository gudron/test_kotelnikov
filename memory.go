package main

import (
	"sort"
	"strings"
)

type bookList []*Book

// Books ...
type Books struct {
	books    bookList
	byID     map[string]*Book
	byName   map[string]bookList
	byAuthor map[string]bookList
}

// addNewBook ...
func (library *Books) addNewBook(book Book) bool {
	_, exist := library.byID[book.id]
	if exist {
		return false
	}

	library.books = append(library.books, &book)

	library.byID[book.id] = &book
	library.byName[book.name] = append(library.byName[book.name], &book)
	library.byAuthor[book.author] = append(library.byAuthor[book.author], &book)

	return true
}

// deleteBook ...
func (library *Books) deleteBook(id string) bool {
	_, exist := library.byID[id]

	if exist {
		position := library.books.findByID(id)

		author := library.books[position].author
		authorBooks := library.byAuthor[author]
		positionInAuthor := authorBooks.findByID(id)

		name := library.books[position].name
		byNameBooks := library.byName[name]
		positionInNames := byNameBooks.findByID(id)

		delete(library.byID, library.books[position].id)
		authorBooks.deleteElement(positionInAuthor)
		byNameBooks.deleteElement(positionInNames)

		library.books.deleteElement(position)

		return true
	}

	return false
}

// listBooksByName ...
func (library *Books) listBooksByName(searchString string) []string {
	result := make([]string, 0)

	for i := 0; i < len(library.books); i++ {
		author := library.books[i].author
		name := library.books[i].name

		contains := strings.Contains(name, searchString)

		if contains {
			if len(library.byName[name]) > 1 {
				result = append(result, author+" - "+name)
			} else {
				result = append(result, name)
			}
		}

	}

	return result
}

// listBooksByAuthor ...
func (library *Books) listBooksByAuthor(searchString string) []string {
	result := make([]string, len(library.books))
	i := 0

	keys := make([]string, 0, len(library.byAuthor))
	for k := range library.byAuthor {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, value := range keys {
		books := library.byAuthor[value]

		for j := 0; j < len(books); j++ {
			author := books[j].author

			contains := strings.Contains(author, searchString)

			if contains {
				result[i] = books[j].name
				i++
			}
		}
	}

	return result
}

// NewLibrary ...
func NewLibrary() *Books {
	books := &Books{}
	books.byID = make(map[string]*Book)
	books.byName = make(map[string]bookList)
	books.byAuthor = make(map[string]bookList)

	return books
}

func (l *bookList) deleteElement(position int) {
	list := *l

	list[position] = list[len(list)-1]
	list[len(list)-1] = &Book{}
	list = list[:len(list)-1]

	l = &list
}

func (l *bookList) findByID(id string) int {
	var position int

	for i := 0; i < len(*l); i++ {
		if (*l)[i].id == id {
			return position
		}
	}

	return -1
}
